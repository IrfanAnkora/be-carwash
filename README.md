# be-carwash

Backend app for carwash application

## Getting started

When pull code from GitLab repository, tou need to have `node version` which is shown in `.nvmrc` file (current version is [16.12.0]). To check your node versions hit `nvm ls` then `nvm use v16.12.0` if have it installed, if not install it.
After having right node version installed and selected, head to projects location and type `npm install` command in terminal in order to install all node modules and packages.

Head to `.env_sample` file to see all environment variables used in this project, and create `.env` file with correct variables and their values in order to start project.

When everything is set up, type `npm run local` in terminal to see if application is up and running. If not, follow error messages to fix errors.

## DB and ODM

My choice of database is `MongoDB` no sql database, since I love to use it, it is easier for me to set it up especially for smaller projects such this one is. I have created database using `MongoDB Atlas` and I have connected to it using connection string generated when creating database. That link should be used in `.env` file as `MONGO_URL` variable.
I am using `mongoose` library to model object data.

## Project arhitecture

Project is using Express framework having folders:

1. routes -> Create auth middleware, in this case JWT authorization
2. controllers -> Receiving requests from Client, validate data received, and pass data to corresponding `service` to handle database and business logic
3. services -> Handling database and business logic, fetching data, processing data, & other operations.
4. models -> Creat Schemas, modeling application data

Beside this folders, there are:

1. core -> Configure application, jwt auth middleware, handling error responses
2. interfaces -> To create interfaces for data transfered through application (for Typescript)
3. functions -> Functions for handling business logic
4. seeds -> Used to seed DB with documents, since tests are using same DB and after tests are executed, the DB is cleared (Test shoul use their own DB but for the sake of this example I am using same database)
5. validations -> Validate data received from Client in controllers

## Validation

I am using AJV validation library. In validation folder you can find validation files for customer and program features. I used this functions to validate all data received from Client: query params & body data.

## Testing

I have set up test using `mocha` Javascript test framework together with `chai` assertion library. I am sorry for not having test coverage of whole application, I was planning to do it but effort for Fronted application extended longer than I planned, so I have just set tests up and created 2-3 tests to show that I am familiar with writing tests.
