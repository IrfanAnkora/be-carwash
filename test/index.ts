import mongoose from 'mongoose';
import customerTests from './customer.test';
import programTests from './program.test';

import Customer from '../src/models/customer.model';
import Program from '../src/models/program.model';

describe(`🚀 Test API`, async () => {
  after(async () => {
    // await Customer.deleteMany();
    // await Program.deleteMany();
    await mongoose.connection.close();
  });
  // await customerTests();
  await programTests();
});
