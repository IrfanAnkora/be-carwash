import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../src/server';

const customerTests = async () => {
  const generateTestCustomer = (username: string) => ({
    firstName: username,
    lastName: username,
    email: `${username}@gmail.com`,
    password: 'Test123!',
    washingCounter: 0,
  });

  const customer1 = {
    _id: '',
    data: generateTestCustomer('customer1'),
  };
  const customer2 = {
    _id: '',
    data: generateTestCustomer('customer2'),
  };
  const customer3 = {
    _id: '',
    data: generateTestCustomer('customer3'),
  };

  chai.use(chaiHttp);

  let server: any;
  describe(`Customer Tests`, async () => {
    before(async () => {
      server = await app;
    });

    const urlPrefix = '/api/v1';

    [customer1, customer2, customer3].map(customer =>
      it(`Should create customer: ${customer.data.email}`, async () => {
        const response = await chai
          .request(server)
          .post(`${urlPrefix}/customers`)
          .send(customer.data);

        chai.expect(response.status).to.equal(200);
        chai.expect(response.body.email).to.equal(customer.data.email);
        chai.expect(response.body.password).not.exist;

        switch (response.body.email) {
          case customer1.data.email:
            customer1._id = response.body._id;
            break;
          case customer2.data.email:
            customer2._id = response.body._id;
            break;
          case customer3.data.email:
            customer3._id = response.body._id;
            break;
        }
      })
    );

    it(`Should login customer: ${customer1.data.email}`, async () => {
      const response = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send(customer1.data);

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.bearerToken).to.be.a('string');
    });

    it(`Should get customer by token having customer info without sensitive data of a customer: ${customer1.data.email}`, async () => {
      const responseLogin = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send(customer1.data);

      const response = await chai
        .request(server)
        .get(`${urlPrefix}/customers/me`)
        .set('Authorization', `Bearer ${responseLogin.body.bearerToken}`)
        .send();
      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.email).to.equal(customer1.data.email);
      chai.expect(response.body.password).not.exist;
    });

    it(`Should change customer's password: ${customer2.data.email}`, async () => {
      const responseLogin = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send(customer2.data);

      const response = await chai
        .request(server)
        .patch(`${urlPrefix}/customers/change-password`)
        .set('Authorization', `Bearer ${responseLogin.body.bearerToken}`)
        .send({
          password: customer2.data.password,
          newPassword: 'Customer2!',
          confirmPassword: 'Customer2!',
        });

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.email).to.equal(customer2.data.email);
      chai.expect(response.body.password).not.exist;
    });

    it(`Should not be able to login with old password: ${customer2.data.email}`, async () => {
      const response = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send({ email: customer2.data.email, password: 'Test123!' });

      chai.expect(response.status).to.equal(401);
      chai.expect(response.body.name).to.equal('LoginFailed');
      chai.expect(response.body.data.message).to.equal('Wrong credentials');
    });

    it(`Should update first and last name of customer: ${customer3.data.email}`, async () => {
      const responseLogin = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send(customer3.data);

      const response = await chai
        .request(server)
        .patch(`${urlPrefix}/customers`)
        .set('Authorization', `Bearer ${responseLogin.body.bearerToken}`)
        .send({
          firstName: 'Carwash3',
          lastName: 'Carwash3',
        });

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.email).to.equal(customer3.data.email);
      chai.expect(response.body.firstName).to.equal('Carwash3');
      chai.expect(response.body.lastName).not.equal(customer3.data.lastName);
      chai.expect(response.body).to.not.have.property('password');
    });

    it(`Should get one customer by id. Customer: ${customer3.data.email}`, async () => {
      const response = await chai
        .request(server)
        .get(`${urlPrefix}/customers/${customer3._id}`)
        .send(customer3.data);

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body._id).to.equal(customer3._id);
      chai.expect(response.body.email).to.equal(customer3.data.email);
      chai.expect(response.body).to.not.have.property('password');
    });

    it(`Should delete customer: ${customer3.data.email}`, async () => {
      const responseLogin = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send(customer3.data);

      const response = await chai
        .request(server)
        .delete(`${urlPrefix}/customers`)
        .set('Authorization', `Bearer ${responseLogin.body.bearerToken}`)
        .send();

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body).to.have.property('message');
      chai
        .expect(response.body.message)
        .to.equal('Customer deleted successfully');
    });
  });
};

export default customerTests;
