import chai from 'chai';
import chaiHttp from 'chai-http';
import app from '../src/server';
import bcrypt from 'bcrypt';

import Customer from '../src/models/customer.model';
import { CAR_TYPE } from '../src/models/program.model';

const programTests = async () => {
  chai.use(chaiHttp);

  const password = 'Test123!';
  const hash = bcrypt.hashSync(password, 10);
  const testCustomer = new Customer({
    _id: '6258af780c52a3cbb37fba20',
    firstName: 'testCustomer',
    lastName: 'testCustomer',
    email: 'testcustomer@gmail.com',
    password: hash,
    washingCounter: 0,
  });

  let server: any;
  describe(`Program Tests`, async () => {
    before(async () => {
      server = await app;
      await testCustomer.save();
    });

    const urlPrefix = '/api/v1';

    it(`Should create customer's ${testCustomer.email} custom program`, async () => {
      console.log('INSIDE testCustomer', testCustomer);

      const responseLogin = await chai
        .request(server)
        .post(`${urlPrefix}/customers/login`)
        .send({
          email: testCustomer.email,
          password,
        });

      const programData = {
        name: 'Test wash program',
        carType: CAR_TYPE.SMALL,
        steps: ['576a6e65677959423962775a'],
      };

      const response = await chai
        .request(server)
        .post(`${urlPrefix}/programs`)
        .set('Authorization', `Bearer ${responseLogin.body.bearerToken}`)
        .send(programData);

      chai.expect(response.status).to.equal(200);
      chai.expect(response.body.name).to.equal(programData.name);
      chai.expect(response.body.carType).to.equal(programData.carType);
      chai.expect(response.body.steps).to.deep.equal(programData.steps);
    });
  });
};

export default programTests;
