import { Request, Response } from 'express';
import { generateFailure } from '../core/errorResponse';
import { stepService } from '../services/step.service';

const getManySteps = async (req: Request, res: Response) => {
  try {
    const allSteps = await stepService.getManySteps();

    return res.send(allSteps);
  } catch (e) {
    console.log(`step.controller in getManySteps method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const getOneStep = async (req: Request, res: Response) => {
  try {
    const stepId: string = req.params.id;
    const step = await stepService.getOneStep(stepId);

    return res.send(step);
  } catch (e) {
    console.log(`step.controller in getOneStep method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

export const stepController = {
  getManySteps,
  getOneStep,
};
