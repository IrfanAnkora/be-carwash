import { Request, Response } from 'express';

import { generateFailure } from '../core/errorResponse';
import { CustomerCreate } from '../interfaces/customer.interface';
import { customerService } from '../services/customer.service';
import customerValidation from '../validations/customer.validation';
import { RequestCustom } from '../interfaces/request.interface';

const createCustomer = async (req: Request, res: Response) => {
  try {
    const data: CustomerCreate = req.body;

    await customerValidation.customerCreate(data);
    const createdCustomer = await customerService.createCustomer(data);

    return res.send(createdCustomer);
  } catch (e) {
    console.log(
      `customer.controller in createCustomer method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const getOneCustomer = async (req: Request, res: Response) => {
  try {
    const customerId: string = req.params.id;
    const customer = await customerService.getOneCustomer(customerId);

    return res.send(customer);
  } catch (e) {
    console.log(
      `customer.controller in getOneCustomer method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const changePassword = async (req: RequestCustom, res: Response) => {
  try {
    const customerId: string = req.customer._id.toString();

    const data = req.body;
    await customerValidation.changePassword(data);
    const customer = await customerService.changePassword(customerId, data);

    return res.send(customer);
  } catch (e) {
    console.log(
      `customer.controller in changePassword method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const deleteCustomer = async (req: RequestCustom, res: Response) => {
  try {
    const customerId: string = req.customer._id.toString();
    const customer = await customerService.deleteCustomer(customerId);

    return res.send(customer);
  } catch (e) {
    console.log(
      `customer.controller in deleteCustomer method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const login = async (req: Request, res: Response) => {
  try {
    const data = req.body;
    const customer = await customerService.login(data);

    return res.send(customer);
  } catch (e) {
    console.log(`customer.controller in login method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const getMe = async (req: RequestCustom, res: Response) => {
  try {
    const customerId: string = req.customer._id.toString();
    const customer = await customerService.getMe(customerId);

    return res.send(customer);
  } catch (e) {
    console.log(`customer.controller in getMe method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const updateCustomer = async (req: RequestCustom, res: Response) => {
  try {
    const customerId: string = req.customer._id.toString();

    const data = req.body;
    await customerValidation.updateCustomer(data);
    const customer = await customerService.updateCustomer(customerId, data);

    return res.send(customer);
  } catch (e) {
    console.log(
      `customer.controller in updateCustomer method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

export const customerController = {
  createCustomer,
  getOneCustomer,
  changePassword,
  deleteCustomer,
  login,
  getMe,
  updateCustomer,
};
