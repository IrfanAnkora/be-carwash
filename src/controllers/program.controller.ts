import { NextFunction, Request, Response } from 'express';
import { RequestCustom } from '../interfaces/request.interface';
import { CarType, ProgramCreate } from '../interfaces/program.interface';
import { generateFailure } from '../core/errorResponse';
import { programService } from '../services/program.service';
import programValidation from '../validations/program.validation';

const getManyPrograms = async (req: RequestCustom, res: Response) => {
  try {
    const { _id, washingCounter } = req.customer;
    await programValidation.getQueryParams(req.query);

    const carType = (req.query as CarType).carType;
    const programType = (req.query as CarType).programType;

    const allPrograms = await programService.getManyPrograms({
      _id,
      washingCounter,
      carType,
      programType,
    });

    return res.send(allPrograms);
  } catch (e) {
    console.log(
      `program.controller in getManyPrograms method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const getCustomPrograms = async (req: RequestCustom, res: Response) => {
  try {
    await programValidation.getQueryParams(req.query);

    const carType = (req.query as CarType).carType;

    const allPrograms = await programService.getCustomPrograms(carType);

    return res.send(allPrograms);
  } catch (e) {
    console.log(
      `program.controller in getCustomPrograms method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const getOneProgram = async (req: RequestCustom, res: Response) => {
  try {
    const { washingCounter } = req.customer;
    const programId: string = req.params.id;
    await programValidation.getQueryParams(req.query);

    const carType = (req.query as CarType).carType;

    const program = await programService.getOneProgram({
      washingCounter,
      programId,
      carType,
    });

    return res.send(program);
  } catch (e) {
    console.log(`program.controller in getOneProgram method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

const createMyProgram = async (req: RequestCustom, res: Response) => {
  try {
    const customerId: string = req.customer._id.toString();
    const data: ProgramCreate = req.body;

    await programValidation.validateProgramCreate(req.body);

    const program = await programService.createMyProgram(customerId, data);

    return res.send(program);
  } catch (e) {
    console.log(
      `program.controller in createMyProgram method error: ${e.name}`
    );
    return generateFailure(res, e);
  }
};

const startProgram = async (req: RequestCustom, res: Response) => {
  try {
    const customer: any = req.customer;
    const programId: string = req.params.id;

    const program = await programService.startProgram(customer, programId);

    return res.send(program);
  } catch (e) {
    console.log(`program.controller in startProgram method error: ${e.name}`);
    return generateFailure(res, e);
  }
};

let clients: any[] = [];
let facts: any[] = [];

const eventsHandler = async (
  request: Request,
  response: Response,
  next: NextFunction
) => {
  console.log(`🟢 eventsHandler 🟢  is listening for events`);
  const headers = {
    'Content-Type': 'text/event-stream',
    Connection: 'keep-alive',
    'Cache-Control': 'no-cache',
  };
  response.writeHead(200, headers);

  const data = `data: ${JSON.stringify(facts)}\n\n`;

  response.write(data);

  const clientId = Date.now();

  const newClient = {
    id: clientId,
    response,
  };

  clients.push(newClient);

  request.on('close', () => {
    console.log(`${clientId} Connection closed`);
    clients = clients.filter(client => client.id !== clientId);
  });
};

const sendEventsToAll = (newFact: any) => {
  clients.forEach(client =>
    client.response.write(`data: ${JSON.stringify(newFact)}\n\n`)
  );
};

const addFact = async (
  request: Request,
  respsonse: Response,
  next: NextFunction
) => {
  const newFact = request.body;
  facts.push(newFact);
  respsonse.json(newFact);
  return sendEventsToAll(newFact);
};

export const programController = {
  getManyPrograms,
  getCustomPrograms,
  getOneProgram,
  createMyProgram,
  startProgram,
  eventsHandler,
  addFact,
};
