import bcrypt from 'bcrypt';

import Customer from '../models/customer.model';
import {
  ICustomer,
  CustomerUpdatePassword,
  CustomerCreate,
} from '../interfaces/customer.interface';
import { throwError } from '../core/errorResponse';
import { signJwt } from '../core/auth.middleware';
import { hideSensitiveData } from '../core/functions';
import { passwordCheck, emailCheck } from '../functions/inputsValidate';

const SALT_ROUNDS = 10;

const createCustomer = async (data: any): Promise<Partial<ICustomer>> => {
  const { firstName, lastName, email, password } = data;
  const customerEmail = email.toLocaleLowerCase();

  const customerExists = await Customer.findOne({ email: customerEmail });
  if (customerExists) {
    return throwError('CustomerAlreadyExists', { email: customerEmail }, 409);
  }

  // Password format check
  const validatePassword = passwordCheck(password);
  if (!validatePassword) {
    return throwError(
      'PasswordFormatNotValid',
      {
        message:
          'min 6 and max 10 letters, at least a symbol, upper and lower case letters and a number',
      },
      409
    );
  }
  // Email format check
  const validateEmail = emailCheck(email);
  if (!validateEmail) {
    return throwError('EmailFormatNotValid', { email: customerEmail }, 409);
  }

  const hash = bcrypt.hashSync(password, SALT_ROUNDS);
  const customer: ICustomer = await Customer.create({
    firstName,
    lastName,
    email: customerEmail,
    password: hash,
    washingCounter: 0,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  });

  return hideSensitiveData(customer);
};

const getOneCustomer = async (
  customerId: string
): Promise<Partial<ICustomer>> => {
  const customer: ICustomer = await Customer.findById(customerId);
  if (!customer) {
    return throwError('CustomerNotFound', { _id: customerId }, 404);
  }

  return hideSensitiveData(customer);
};

const changePassword = async (
  customerId: string,
  data: CustomerUpdatePassword
): Promise<any> => {
  const { password, newPassword, confirmPassword } = data;

  if (!password) {
    return throwError(
      'PasswordIsRequired',
      { message: 'Please provide password' },
      409
    );
  }

  if (!newPassword) {
    return throwError(
      'NewPasswordIsRequired',
      { message: 'Please provide new password' },
      409
    );
  }

  if (!confirmPassword) {
    return throwError(
      'ConfirmPasswordIsRequired',
      { message: 'Please confirm new password' },
      409
    );
  }

  if (newPassword !== confirmPassword) {
    return throwError(
      'PasswordNotMatch',
      { message: 'You must confirm new password' },
      409
    );
  }
  const customerInfo: any = await Customer.findById(customerId);
  const comparedPassword = bcrypt.compareSync(password, customerInfo.password);
  if (!comparedPassword) {
    return throwError('Failed', { message: 'Wrong credentials' }, 401);
  }

  const newHash = bcrypt.hashSync(newPassword, SALT_ROUNDS);
  const customer: ICustomer = await Customer.findOneAndUpdate(
    { _id: customerId },
    { password: newHash, updatedAt: Date.now() },
    { new: true }
  );

  return hideSensitiveData(customer);
};

const deleteCustomer = async (
  customerId: string
): Promise<Record<string, any>> => {
  await Customer.deleteOne({ _id: customerId });
  return { message: 'Customer deleted successfully' };
};

const login = async (data: CustomerCreate): Promise<Record<string, any>> => {
  const { email, password } = data;
  const customerEmail = email.toLocaleLowerCase();

  const customer: ICustomer = await Customer.findOne({ email: customerEmail });
  if (!customer) {
    return throwError(
      'CustomerNotExist',
      { message: "CustomerDoesn'tExist" },
      404
    );
  }

  const comparedPassword = bcrypt.compareSync(password, customer.password);
  if (!comparedPassword) {
    return throwError('LoginFailed', { message: 'Wrong credentials' }, 401);
  }
  const bearerToken: string = await signJwt(customer);

  return {
    bearerToken,
  };
};

const getMe = async (customerId: string): Promise<Partial<ICustomer>> => {
  const customer: ICustomer = await Customer.findById(customerId);
  return hideSensitiveData(customer);
};

const updateCustomer = async (
  customerId: string,
  data: Partial<CustomerCreate>
): Promise<Partial<ICustomer>> => {
  const { firstName, lastName } = data;

  const customer: ICustomer = await Customer.findOneAndUpdate(
    { _id: customerId },
    { firstName, lastName, updatedAt: Date.now() },
    { new: true }
  );

  return hideSensitiveData(customer);
};

export const customerService = {
  createCustomer,
  getOneCustomer,
  changePassword,
  deleteCustomer,
  login,
  getMe,
  updateCustomer,
};
