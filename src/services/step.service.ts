import Step from '../models/step.model';
import { IStep } from '../interfaces/step.interface';

const getManySteps = async (): Promise<IStep[]> => {
  return await Step.find();
};

const getOneStep = async (stepId: string): Promise<IStep> => {
  return await Step.findById(stepId);
};

export const stepService = {
  getManySteps,
  getOneStep,
};
