import Program from '../models/program.model';
import { throwError } from '../core/errorResponse';
import {
  calculatePrice,
  calculateTime,
  calculateByCarType,
} from '../functions/calculations';
import { priceDiscount } from '../functions/discounts';
import {
  IProgram,
  PROGRAMS,
  ProgramCreate,
  PROGRAM_TYPE,
} from '../interfaces/program.interface';
import { IStep } from '../interfaces/step.interface';
import Customer from '../models/customer.model';
import { generateProgramInfo } from '../functions/generateCurl';
import { CAR_TYPE } from '../models/program.model';

const getManyPrograms = async ({
  _id,
  washingCounter,
  carType,
  programType,
}: {
  _id: string;
  washingCounter: number;
  carType: CAR_TYPE;
  programType: PROGRAM_TYPE;
}): Promise<IProgram[]> => {
  // Should get 3 default { customer: null }, and all my custom programs { customer: customerId }
  let query = {};
  switch (programType) {
    case PROGRAM_TYPE.DEFAULT:
      query = {
        customer: null,
      };
      break;
    case PROGRAM_TYPE.MY:
      query = {
        customer: _id,
      };
      break;

    default:
      query = {
        $or: [
          {
            customer: null,
          },
          {
            customer: _id,
          },
        ],
      };
      break;
  }
  let programs: IProgram[] = await Program.find({
    ...query,
  })
    .populate('steps')
    .lean();

  // Check if eliglible for discount, if so calculate new price
  const isEligible = washingCounter % 10;
  programs = programs.map((program: IProgram) => {
    let price: number = calculateByCarType(carType, program.price);
    if (washingCounter > 0 && isEligible === 0) {
      price = priceDiscount(washingCounter, price);
    }
    return { ...program, price, carType };
  });

  return programs;
};

const getCustomPrograms = async (carType: CAR_TYPE): Promise<IProgram[]> => {
  // Should get 3 default { customer: null } without customer programs
  let programs: IProgram[] = await Program.find({
    customer: null,
  })
    .populate('steps')
    .lean();

  programs = programs.map((program: IProgram) => {
    let price: number = calculateByCarType(carType, program.price);
    return { ...program, price, carType };
  });

  return programs;
};

const getOneProgram = async ({
  washingCounter,
  programId,
  carType,
}: {
  washingCounter: number;
  programId: string;
  carType: CAR_TYPE;
}): Promise<IProgram> => {
  const program: IProgram = await Program.findById(programId)
    .populate('steps')
    .lean();

  let price: number = program.price;
  // Check if eliglible for discount, if so calculate new price
  const isEligible = washingCounter % 10;
  if (washingCounter > 0 && isEligible === 0) {
    price = priceDiscount(washingCounter, price);
  }
  price = calculateByCarType(carType, price);

  return { ...program, price, carType };
};

const createMyProgram = async (
  customerId: string,
  data: ProgramCreate
): Promise<IProgram> => {
  const { carType, steps } = data;
  let name = data.name;
  if (!name) {
    name = PROGRAMS.CUSTOM;
  }
  // Customer must include at least one step in order to create custom program
  if (steps.length === 0) {
    return throwError(
      'MustIncludeAtLeastOne',
      { message: 'You must include at least one step' },
      409
    );
  }

  // Check if customer selected "High pressure washing" step
  const hasWashingStep = steps.includes('576a6e65677959423962775a');
  if (!hasWashingStep) {
    return throwError(
      'MustIncludeWashingStep',
      { message: 'You must include "High pressure washing" step' },
      409
    );
  }

  // Check if customer has less then 6 Custom programs
  const totalCustomerPrograms = await Program.find({
    customer: customerId,
  }).count();

  if (totalCustomerPrograms === 5) {
    return throwError(
      'MaxLimitReached',
      { message: 'You can have only 5 custom programs' },
      409
    );
  }

  const price = await calculatePrice(carType, steps);
  const time = await calculateTime(steps);
  return await Program.create({
    name,
    carType,
    price,
    time,
    customer: customerId,
    steps,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  });
};

const startProgram = async (customer: any, programId: string): Promise<any> => {
  let { _id, washingCounter } = customer;
  const customerId: string = _id.toString();

  washingCounter = customer.washingCounter + 1;

  // When ever customer starts program. it means he purichased it, and increments his washingCounter
  await Customer.findOneAndUpdate(
    { _id: customerId },
    { washingCounter, updatedAt: Date.now() }
  );

  const selectedProgram: IProgram = await Program.findById(programId).populate(
    'steps'
  );

  await generateProgramInfo(selectedProgram.steps as unknown as IStep[]);
  return selectedProgram;
};

export const programService = {
  getManyPrograms,
  getCustomPrograms,
  getOneProgram,
  createMyProgram,
  startProgram,
};
