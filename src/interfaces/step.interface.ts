import { ObjectId } from 'mongodb';
import { WASH_TYPE } from '../models/step.model';

export interface IStep {
  name: string;
  price: number;
  time: number;
  washingType: WASH_TYPE;
  order: number;
  createdAt: string;
  updatedAt: string;
}

export interface StepSeed {
  _id: ObjectId;
  name: string;
  price: number;
  time: number;
  washingType: WASH_TYPE;
  order: number;
  createdAt: string;
  updatedAt: string;
}
