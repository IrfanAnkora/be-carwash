export interface ErrorType {
  name: string;
  data: any;
  statusCode: number;
}
