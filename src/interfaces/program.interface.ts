import { ObjectId } from 'mongodb';
import { CAR_TYPE } from '../models/program.model';

export interface IProgram {
  name: string;
  carType: CAR_TYPE;
  price: number;
  time: number;
  customer: string;
  steps: [string];
  createdAt: string;
  updatedAt: string;
}

export interface ProgramSeed {
  _id: ObjectId;
  name: string;
  carType: CAR_TYPE;
  price: number;
  time: number;
  customer: string | null;
  steps: [string] | [];
  createdAt: string;
  updatedAt: string;
}

export enum PROGRAM_TYPE {
  ALL = 'all',
  MY = 'my',
  DEFAULT = 'default',
}

export type CarType = { carType?: CAR_TYPE; programType?: PROGRAM_TYPE };

export enum PROGRAMS {
  CLASSIC = 'Wash Classic',
  PLUS = 'Wash Classic Plus',
  PREMIUM = 'Wash Premium',
  CUSTOM = 'Wash Custom',
}

export interface ProgramCreate {
  name?: string;
  carType: CAR_TYPE;
  steps: [string] | [];
}
