import { ObjectId } from 'mongodb';

export interface ICustomer {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  washingCounter: number;
  createdAt: string;
  updatedAt: string;
}

export interface CustomerCreate {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
}

export interface CustomerSeed {
  _id: ObjectId;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  washingCounter: number;
  createdAt: string;
  updatedAt: string;
}

export interface CustomerUpdatePassword {
  password: string;
  newPassword: string;
  confirmPassword: string;
}
