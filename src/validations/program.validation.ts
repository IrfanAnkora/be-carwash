import _ from 'lodash';
import Ajv from 'ajv';
import { throwError } from '../core/errorResponse';
import { ErrorType } from '../interfaces/error.interface';
import { CarType } from '../interfaces/program.interface';

const ajv = new Ajv();

const getQueryParams = async (data: CarType): Promise<ErrorType> => {
  const objectForUpdate = _.pick(data, ['carType']);
  const schema = {
    type: 'object',
    properties: {
      carType: { enum: ['small', 'medium', 'large'] },
    },
    required: ['carType'],
    additionalProperties: false,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForUpdate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};

const validateProgramCreate = async (data: any): Promise<ErrorType> => {
  const objectForUpdate = _.pick(data, ['name', 'carType', 'steps']);
  const schema = {
    type: 'object',
    properties: {
      carType: { enum: ['small', 'medium', 'large'] },
      steps: { type: 'array', minItems: 1 },
    },
    required: ['carType', 'steps'],
    additionalProperties: true,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForUpdate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};

const programValidation = {
  getQueryParams,
  validateProgramCreate,
};

export default programValidation;
