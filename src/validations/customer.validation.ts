import _ from 'lodash';
import Ajv from 'ajv';
import { CustomerCreate } from '../interfaces/customer.interface';
import { throwError } from '../core/errorResponse';
import { ErrorType } from '../interfaces/error.interface';

const ajv = new Ajv({ allErrors: true, $data: true });

const customerCreate = async (data: CustomerCreate): Promise<ErrorType> => {
  const objectForCreate = _.pick(data, [
    'firstName',
    'lastName',
    'password',
    'email',
  ]);
  const schema = {
    type: 'object',
    properties: {
      firstName: { type: 'string' },
      lastName: { type: 'string' },
      email: { type: 'string' },
      password: { type: 'string' },
    },
    required: ['firstName', 'lastName', 'email', 'password'],
    additionalProperties: false,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForCreate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};

const changePassword = async (data: CustomerCreate): Promise<ErrorType> => {
  const objectForUpdate = _.pick(data, [
    'password',
    'newPassword',
    'confirmPassword',
  ]);
  const schema = {
    type: 'object',
    properties: {
      password: { type: 'string' },
      newPassword: { type: 'string' },
      confirmPassword: {
        const: {
          $data: '1/newPassword',
          messages: 'newPassword and confirmPassword must match',
        },
      },
    },
    required: ['password', 'newPassword', 'confirmPassword'],
    additionalProperties: false,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForUpdate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};
const updateCustomer = async (data: CustomerCreate): Promise<ErrorType> => {
  const objectForUpdate = _.pick(data, ['firstName', 'lastName']);
  const schema = {
    type: 'object',
    properties: {
      firstName: { type: 'string' },
      lastName: { type: 'string' },
    },
    required: ['firstName', 'lastName'],
    additionalProperties: false,
  };

  const validate = ajv.compile(schema);
  const valid = validate(objectForUpdate);

  if (!valid) {
    const error = validate.errors[0];
    return throwError(error.message, error.params, 400);
  }
};

const customerValidation = {
  customerCreate,
  changePassword,
  updateCustomer,
};

export default customerValidation;
