import _ from 'lodash';

export const hideSensitiveData = (customer: any) => {
  return _.omit(JSON.parse(JSON.stringify(customer)), ['password']);
};
