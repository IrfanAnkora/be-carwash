import { Schema, Model, model } from 'mongoose';
import { ICustomer } from '../interfaces/customer.interface';

const customerSchema: Schema = new Schema(
  {
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    washingCounter: {
      type: Number,
      default: 0,
      required: true,
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

const Customer: Model<ICustomer> = model('Customer', customerSchema);

export default Customer;
