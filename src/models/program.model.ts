import { Schema, Model, model } from 'mongoose';
import { IProgram } from '../interfaces/program.interface';

export enum CAR_TYPE {
  SMALL = 'small',
  MEDIUM = 'medium',
  LARGE = 'large',
}

const programSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    carType: {
      type: String,
      required: true,
      enum: CAR_TYPE,
      default: CAR_TYPE.SMALL,
    },
    price: {
      type: Number,
      required: true,
      default: 0.0,
    },
    time: {
      type: Number,
      required: true,
      default: 0,
    },
    customer: {
      type: Schema.Types.ObjectId,
      ref: 'Customer',
    },
    steps: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Step',
      },
    ],
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

const Program: Model<IProgram> = model('Program', programSchema);

export default Program;
