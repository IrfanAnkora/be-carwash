import { Schema, Model, model } from 'mongoose';
import { IStep } from '../interfaces/step.interface';

export enum WASH_TYPE {
  CARE = 'care',
  WASH = 'wash',
}

const stepSchema: Schema = new Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    price: {
      type: Number,
      required: true,
      default: 0.0,
    },
    time: {
      type: Number,
      required: true,
      default: 0,
    },
    washingType: {
      type: String,
      required: true,
      enum: WASH_TYPE,
      default: WASH_TYPE.WASH,
    },
    order: {
      type: Number,
      required: true,
      default: 0,
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
  },
  { versionKey: false }
);

const Step: Model<IStep> = model('Step', stepSchema);

export default Step;
