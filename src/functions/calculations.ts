import mongoose from 'mongoose';
import Step from '../models/step.model';

import { CONFIG } from '../core/config';
import { CAR_TYPE } from '../models/program.model';
import { IStep } from '../interfaces/step.interface';

const fetchSteps = async (): Promise<IStep[]> => {
  await mongoose.connect(CONFIG.DB_URL || '');
  return await Step.find();
};

export const calculateByCarType = (
  carType: CAR_TYPE,
  totalStepsPrice: number
): number => {
  switch (carType) {
    case CAR_TYPE.SMALL:
      return Number(
        (totalStepsPrice - (totalStepsPrice * 20) / 100).toFixed(2)
      );
    case CAR_TYPE.LARGE:
      return Number(
        (totalStepsPrice + (totalStepsPrice * 30) / 100).toFixed(2)
      );
    default:
      return Number(totalStepsPrice.toFixed(2));
  }
};

export const calculatePrice = async (
  carType: CAR_TYPE,
  steps: string[]
): Promise<number> => {
  const fetchedSteps = await fetchSteps();

  let totalStepsPrice = 0;
  steps.forEach(element => {
    fetchedSteps.filter((step: any) => {
      if (step._id == element) {
        totalStepsPrice += step.price;
      }
    });
  });

  return calculateByCarType(carType, totalStepsPrice);
};

export const calculateTime = async (steps: string[]): Promise<number> => {
  const fetchedSteps = await fetchSteps();

  let totalTime = 0;
  steps.forEach(element => {
    fetchedSteps.filter((step: any) => {
      if (step._id == element) {
        totalTime += step.time;
      }
    });
  });

  return totalTime;
};
