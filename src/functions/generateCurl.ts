import { exec } from 'child_process';
import { IStep } from '../interfaces/step.interface';

const executeCurl = async (
  url: string,
  data: string,
  method: string = 'POST',
  type: string = 'Content-Type: application/json'
): Promise<void> => {
  const command = `curl -X ${method} \
  -H "${type}" \
  -d '{"info": ${data}}'\
  -s ${url}`;

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.log(`error: ${error.message}`);
      return;
    }
    if (stderr) {
      console.log(`stderr: ${stderr}`);
      return;
    }
    console.log(`stdout: ${stdout}`);
  });
};

const generateProgramCurl = async (messages: any[]): Promise<void> => {
  const url = `http://localhost:4000/api/v1/programs/fact`;
  let time = 0;
  messages.forEach(async el => {
    time += el.data.currentStep.time * 1000;

    setTimeout(async () => {
      await executeCurl(url, JSON.stringify(el.data));
    }, time);
  });
};

export const generateProgramInfo = async (steps: IStep[]): Promise<void> => {
  const programInfoCollection = [];

  if (steps.length === 1) {
    const info = {
      data: {
        previousStep: {
          name: 'PREVIOUS STEP: Program started',
          time: 0,
        },
        currentStep: {
          name: `CURRENT STEP: ${steps[0].name}`,
          time: steps[0].time,
        },
        nextStep: {
          name: 'NEXT STEP: Program ends',
          time: 0,
        },
      },
    };
    programInfoCollection.push(info);
  } else {
    for (let i = 0; i < steps.length; i++) {
      if (i === 0) {
        const info = {
          data: {
            previousStep: {
              name: 'PREVIOUS STEP: Program started',
              time: 0,
            },
            currentStep: {
              name: `CURRENT STEP: ${steps[i].name}`,
              time: steps[i].time,
            },
            nextStep: {
              name: `NEXT STEP: ${steps[i + 1].name}`,
              time: steps[i + 1].time,
            },
          },
        };
        programInfoCollection.push(info);
      } else {
        const info = {
          data: {
            previousStep: {
              name: `PREVIOUS STEP: ${steps[i - 1].name}`,
              time: steps[i - 1].time,
            },
            currentStep: {
              name: `CURRENT STEP: ${steps[i].name}`,
              time: steps[i].time,
            },
            nextStep: {},
          },
        };
        if (i + 1 != steps.length) {
          info.data.nextStep = {
            name: `NEXT STEP: ${steps[i + 1].name}`,
            time: steps[i + 1].time,
          };
        } else {
          info.data.nextStep = {
            name: 'NEXT STEP: Program ends',
            time: 0,
          };
        }
        programInfoCollection.push(info);
      }
    }
  }

  await generateProgramCurl(programInfoCollection);
};
