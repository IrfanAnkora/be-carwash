export const loyalityDiscount = (washingCounter: number): number => {
  // Check if washingCounter is 10th run
  const discount: number = washingCounter % 10 === 0 ? 10 : 0;
  // Add additional discount on every 10th run, except very first 10th run
  const additionalDisc: number =
    discount !== 0 && washingCounter !== 10 ? washingCounter / 10 : 0;
  // Sum 10th run discount and additional discount on every 10th run
  const sum: number = Number(discount) + Number(additionalDisc);
  // If total discount exceeds 50%, then set it to 50, else set as it is
  const totalDiscount: number = sum > 50 ? 50 : sum;

  return totalDiscount;
};

export const priceDiscount = (
  washingCounter: number,
  programPrice: number
): number => {
  // Get total discount percentage number
  const discount: number = loyalityDiscount(washingCounter);
  // Calculate new discounted price
  const discounted = programPrice - (programPrice * discount) / 100;

  return discounted;
};
