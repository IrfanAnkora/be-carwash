import { Router, Request, Response } from 'express';

import customerRoutes from './customer.routes';
import programRoutes from './program.routes';
import stepRoutes from './step.routes';

const router = Router();

router.get('/test', (req: Request, res: Response) => {
  try {
    res.send({ message: `🟢 TEST: Application is working.` });
  } catch (e) {
    console.error(`🛑 TEST: Server error testing application.`, e);
    res.status(500).send(e);
  }
});

router.use('/customers', customerRoutes);
router.use('/programs', programRoutes);
router.use('/steps', stepRoutes);

export default router;
