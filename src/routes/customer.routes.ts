import { Router } from 'express';

import { customerController } from '../controllers/customer.controller';
import { authenticateJWT } from '../core/auth.middleware';

const customerRoutes = Router();

customerRoutes.route('/me').get(authenticateJWT, customerController.getMe);

customerRoutes.route('/').post(customerController.createCustomer);

customerRoutes
  .route('/change-password')
  .patch(authenticateJWT, customerController.changePassword);

customerRoutes
  .route('/')
  .patch(authenticateJWT, customerController.updateCustomer);

customerRoutes
  .route('/')
  .delete(authenticateJWT, customerController.deleteCustomer);

customerRoutes.route('/:id').get(customerController.getOneCustomer);

customerRoutes.route('/login').post(customerController.login);

export default customerRoutes;
