import { Router } from 'express';
import { programController } from '../controllers/program.controller';
import { authenticateJWT } from '../core/auth.middleware';

const programRoutes = Router();

programRoutes.route('/events').get(programController.eventsHandler);
programRoutes.route('/fact').post(programController.addFact);
programRoutes
  .route('/start/:id')
  .get(authenticateJWT, programController.startProgram);

programRoutes.route('/custom').get(programController.getCustomPrograms);

programRoutes
  .route('/')
  .get(authenticateJWT, programController.getManyPrograms);

programRoutes
  .route('/')
  .post(authenticateJWT, programController.createMyProgram);

programRoutes
  .route('/:id')
  .get(authenticateJWT, programController.getOneProgram);

export default programRoutes;
