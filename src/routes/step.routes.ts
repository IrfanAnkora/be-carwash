import { Router } from 'express';
import { stepController } from '../controllers/step.controller';

const stepRoutes = Router();

stepRoutes.route('/').get(stepController.getManySteps);

stepRoutes.route('/:id').get(stepController.getOneStep);

export default stepRoutes;
