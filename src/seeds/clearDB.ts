import mongoose from 'mongoose';
import { throwError } from '../core/errorResponse';
import { CONFIG } from '../core/config';

const clearDB = async () => {
  try {
    await mongoose.connect(CONFIG.DB_URL || '');
    const collections = await mongoose.connection.db
      .listCollections()
      .toArray();

    const promises: any[] = [];
    collections.forEach((e, i, a) => {
      promises.push(mongoose.connection.db.dropCollection(e.name));
    });

    await Promise.all(promises).then(() => {
      mongoose.connection.close();
    });

    console.log(`✅ Clearing DB finished successfully.`);
    return { status: 'Dropped DB collections' };
  } catch (e) {
    console.log(`Clearing DB problem ${JSON.stringify(e)}`);
    return throwError('Clearing DB problem', e.name, 500);
  }
};

clearDB();
