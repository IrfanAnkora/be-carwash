import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

import { throwError } from '../core/errorResponse';
import Program from '../models/program.model';
import { CONFIG } from '../core/config';
import { ProgramSeed, PROGRAMS } from '../interfaces/program.interface';
import { CAR_TYPE } from '../models/program.model';

export const programsSeed = async (): Promise<ProgramSeed[]> => {
  try {
    await mongoose.connect(CONFIG.DB_URL || '');

    const programs = [
      {
        _id: new ObjectId('61a89caa38ebd1e034649e10'),
        name: PROGRAMS.CLASSIC,
        carType: CAR_TYPE.MEDIUM,
        price: 10.0,
        time: 20,
        customer: null as any,
        steps: [
          '374e31614d58444f5165576d',
          '576a6e65677959423962775a',
          '374c44647778395977653159',
          '676c39617677715872624731',
          '774d76626d7742306559416c',
        ],
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('61a89caa38ebd1e034649e11'),
        name: PROGRAMS.PLUS,
        carType: CAR_TYPE.MEDIUM,
        price: 20.0,
        time: 25,
        customer: null,
        steps: [
          '374e31614d58444f5165576d',
          '576a6e65677959423962775a',
          '374c44647778395977653159',
          '676c39617677715872624731',
          '61b912ea02b5fc911736a859',
          '774d76626d7742306559416c',
        ],
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('61a89caa38ebd1e034649e12'),
        name: PROGRAMS.PREMIUM,
        carType: CAR_TYPE.MEDIUM,
        price: 30.0,
        time: 35,
        customer: null,
        steps: [
          '374e31614d58444f5165576d',
          '576a6e65677959423962775a',
          '374c44647778395977653159',
          '676c39617677715872624731',
          '57706d626b67514a58627a4a',
          '61b912ea02b5fc911736a859',
          '774d76626d7742306559416c',
        ],
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('6250c08a48b7336983b32f15'),
        name: PROGRAMS.CUSTOM,
        carType: CAR_TYPE.MEDIUM,
        price: 0.0,
        time: 0,
        customer: '6250bfa5dbc19661a5bf2da7',
        steps: ['576a6e65677959423962775a'],
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
    ];

    await Promise.all([Program.insertMany(programs)]);

    console.log(`✅ Seedinng programs finished successfully.`);
    mongoose.connection.close();
  } catch (e) {
    console.log(`Couldn't seed programs ${e}`);
    return throwError('SeedingProgramsError', e, 500);
  }
};
