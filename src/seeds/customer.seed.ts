import bcrypt from 'bcrypt';
import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

import { throwError } from '../core/errorResponse';
import Customer from '../models/customer.model';
import { CONFIG } from '../core/config';
import { CustomerSeed } from '../interfaces/customer.interface';

export const customersSeed = async (): Promise<CustomerSeed[]> => {
  try {
    await mongoose.connect(CONFIG.DB_URL || '');

    const customers = [
      {
        _id: new ObjectId('6250bfa5dbc19661a5bf2da7'),
        firstName: 'Test',
        lastName: 'Test',
        email: 'test@gmail.com',
        password: 'Test1!',
        washingCounter: 0,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('6250c072f987f607360e737a'),
        firstName: 'Irfan',
        lastName: 'Mehanovic',
        email: 'irfan.mehanovic1@gmail.com',
        password: 'Irfo1!',
        washingCounter: 0,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('6250c08a48b7336983b32f14'),
        firstName: 'Admin',
        lastName: 'Admin',
        email: 'admin@gmail.com',
        password: 'Admin1!',
        washingCounter: 0,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
    ];

    const saltRounds = 10;
    const customerPromises: any[] = [];

    customers.map((customer: CustomerSeed) => {
      const hash = bcrypt.hashSync(customer.password, saltRounds);
      customerPromises.push(Customer.create({ ...customer, password: hash }));
    });

    await Promise.all(customerPromises);

    console.log(`✅ Seedinng customers finished successfully.`);
    mongoose.connection.close();
  } catch (e) {
    console.log(`Couldn't seed customers ${e}`);
    return throwError('SeedingCustomersError', e, 500);
  }
};
