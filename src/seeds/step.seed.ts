import mongoose from 'mongoose';
import { ObjectId } from 'mongodb';

import { throwError } from '../core/errorResponse';
import Step, { WASH_TYPE } from '../models/step.model';
import { CONFIG } from '../core/config';
import { StepSeed } from '../interfaces/step.interface';

export const stepsSeed = async (): Promise<StepSeed[]> => {
  try {
    await mongoose.connect(CONFIG.DB_URL || '');

    const steps = [
      {
        _id: new ObjectId('374e31614d58444f5165576d'),
        name: 'Active foam',
        price: 2.0,
        time: 3,
        washingType: WASH_TYPE.WASH,
        order: 1,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('576a6e65677959423962775a'),
        name: 'High pressure washing',
        price: 3.0,
        time: 5,
        washingType: WASH_TYPE.WASH,
        order: 2,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('374c44647778395977653159'),
        name: 'Brushes and shampoo',
        price: 5.0,
        time: 5,
        washingType: WASH_TYPE.WASH,
        order: 3,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('676c39617677715872624731'),
        name: 'Wheels and rims',
        price: 3.0,
        time: 3,
        washingType: WASH_TYPE.WASH,
        order: 4,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('57706d626b67514a58627a4a'),
        name: 'Washing the chassis',
        price: 5.0,
        time: 5,
        washingType: WASH_TYPE.WASH,
        order: 5,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('61b912ea02b5fc911736a859'),
        name: 'Wax and polishing',
        price: 8.0,
        time: 10,
        washingType: WASH_TYPE.CARE,
        order: 6,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
      {
        _id: new ObjectId('774d76626d7742306559416c'),
        name: 'Drying',
        price: 3.0,
        time: 4,
        washingType: WASH_TYPE.CARE,
        order: 7,
        createdAt: Date.now().toString(),
        updatedAt: Date.now().toString(),
      },
    ];

    await Promise.all([Step.insertMany(steps)]);

    console.log(`✅ Seedinng steps finished successfully.`);
    mongoose.connection.close();
  } catch (e) {
    console.log(`Couldn't seed steps ${e}`);
    return throwError('SeedingStepsError', e, 500);
  }
};
