import { throwError } from '../core/errorResponse';
import { customersSeed } from './customer.seed';
import { programsSeed } from './program.seed';
import { stepsSeed } from './step.seed';

const seedHandler = async () => {
  try {
    await customersSeed();
    await programsSeed();
    await stepsSeed();

    return { status: 'ok' };
  } catch (e) {
    console.log(`Problem with seeding ${JSON.stringify(e)}`);
    return throwError('Problem with seeding', e.name, 500);
  }
};

seedHandler();
